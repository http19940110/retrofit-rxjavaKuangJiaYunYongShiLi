package com.black.board.base;

import android.app.Application;
import android.util.Log;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;


/**
 * Created by gaojun on 2017/7/17.
 */

public class BbApplication extends Application {
    public OkHttpClient client;
    private static BbApplication app;

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(new HttpLoggingInterceptor.Logger() {
            @Override
            public void log(String message) {
                //打印retrofit日志
                Log.i("retrofitBack = ", "" + message);
            }
        });
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        client = new OkHttpClient.Builder().addInterceptor(loggingInterceptor).build();
    }

    public static BbApplication getInstance() {
        return app;
    }

    public OkHttpClient getClient() {
        return client;
    }

}
