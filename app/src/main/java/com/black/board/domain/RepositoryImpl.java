package com.black.board.domain;


/**
 *rxJava可以将json串对象转换为任意的类型的集合
 */

import com.black.board.bean.Person;
import com.black.board.bean.Result;
import com.black.board.bean.UserBean;
import com.black.board.http.ApiManager;
import com.black.board.rx.ResultTransform;
import com.black.board.utils.schedulers.SchedulerProvider;

import java.util.List;

import rx.Observable;
import rx.functions.Func1;

/**
 * 接口实现类
 */

public class RepositoryImpl implements Repository {

    //将接口传入进来
    private ApiManager apiManager;
    private SchedulerProvider schedulerProvider;


    public RepositoryImpl(ApiManager apiManager, SchedulerProvider schedulerProvider) {
        this.apiManager = apiManager;
        this.schedulerProvider = schedulerProvider;
    }


    //获取电影数据 不进行异常拦截
    @Override
    public Observable<List<Person>> getMovie(int start, int count) {
        return apiManager.getMovie(start, count).flatMap(new Func1<Result<List<Person>>,
                Observable<List<Person>>>() {
            @Override
            public Observable<List<Person>> call(Result<List<Person>> listResult) {
                return Observable.just(listResult.getData());
            }
        }).subscribeOn(schedulerProvider.io());
    }

    //注册 需要进行异常拦截 参与小黑板开发的用这个形式
    @Override
    public Observable<String> regUserInfo(UserBean str) {
        return apiManager.register(str).flatMap(new ResultTransform<String>())
                .subscribeOn(schedulerProvider.io());
    }

}
