package com.black.board.domain;

import com.black.board.bean.Person;
import com.black.board.bean.UserBean;

import java.util.List;

import rx.Observable;

/**
 * 方法封装接口
 */
public interface Repository {

    //请求范例
    Observable<List<Person>> getMovie(int start, int count);

    //注册为用户
    Observable<String> regUserInfo(UserBean str);

}
