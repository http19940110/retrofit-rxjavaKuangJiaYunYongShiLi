package com.black.board.ui.home;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.amap.api.maps2d.AMap;
import com.amap.api.maps2d.CameraUpdateFactory;
import com.amap.api.maps2d.LocationSource;
import com.amap.api.maps2d.MapView;
import com.amap.api.maps2d.model.MyLocationStyle;
import com.black.board.R;
import com.black.board.base.BaseActivity;
import com.black.board.bean.Person;
import com.black.board.dagger.DaggerBean;
import com.black.board.dagger.provide.Flower;
import com.black.board.dagger.provide.Pot;
import com.black.board.http.ApiManager;
import com.black.board.http.ServiceManager;
import com.black.board.rx.BaseSubscriber;
import com.black.board.utils.schedulers.SchedulerProvider;

import java.util.List;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Subscriber;


public class MainActivity extends BaseActivity implements LocationSource, AMapLocationListener {



    MapView mMapView = null;
    AMap aMap;
    private OnLocationChangedListener mListener;
    private AMapLocationClient mlocationClient;
    private AMapLocationClientOption mLocationOption;

    ServiceManager sm=ServiceManager.getInstance();
    SchedulerProvider sd=sm.getSchedulerProvider();
    ApiManager manager=sm.getApiManager();

//    @InjectView(R.id.tv)
//    TextView reg;


    //注入两个实例一个是抽象类一个是正常类
    //直接在构造方法里运用@Inject
    @Inject
    DaggerBean daggerBean;
    //抽象类的时候用的
    //用的@Provider提供的 @Module
    @Inject
    Pot pot;


    //当两个类都是继承同一个父类的时候需要做区别用：@Qualifier是限定符，而@Named则是基于String的限定符


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        //获取地图控件引用
        mMapView = (MapView) findViewById(R.id.map);
        //在activity执行onCreate时执行mMapView.onCreate(savedInstanceState)，创建地图
        mMapView.onCreate(savedInstanceState);
        if (aMap == null) {
            aMap = mMapView.getMap();
        }
        aMap.moveCamera(CameraUpdateFactory.zoomTo(15f));
        setUpMap();

         DaggerMainActivityComponent.create().inject(this);
         String show=  daggerBean.show();//减少两个实例的实例化
         String mm=  pot.show();

         Log.d("",show+mm);
        //小黑板请求注册 需要把baseUrl修改  ServiceManager类
//        reg.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                UserBean userBean=new UserBean();
//                userBean.setLoginName("1234567");
//                userBean.setLoginPwd("1234567");
//                sm.getRepository(manager, sd).regUserInfo(userBean).observeOn(sd.ui()).
//                        subscribe(new RegisterSubscriber());
//            }
//        });




    }

    //注册网络回调
    private class RegisterSubscriber extends BaseSubscriber<String>{
        @Override
        public void onError(Throwable e) {
            super.onError(e);
            Toast.makeText(MainActivity.this,"注册失败"+e.toString(),Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onNext(String s) {
            super.onNext(s);
            //注册成功
            Toast.makeText(MainActivity.this,"注册成功"+s,Toast.LENGTH_SHORT).show();
        }
    }


    //范例请求电影数据
    @OnClick(R.id.tvMovies)
    void getMovie(){
        sm.getRepository(manager,sd)
                .getMovie(0,1).observeOn(sd.ui())
                .subscribe(new Subscriber<List<Person>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(List<Person> person) {
                        Toast.makeText(MainActivity.this,"成功"+person.toString(),Toast.LENGTH_LONG).show();
                    }

                });
    }



    /**
     * 设置一些amap的属性
     */
    private void setUpMap() {
        // 自定义系统定位小蓝点
        MyLocationStyle myLocationStyle = new MyLocationStyle();
//        myLocationStyle.myLocationIcon(BitmapDescriptorFactory
//                .fromResource(R.drawable.location_marker));// 设置小蓝点的图标
        myLocationStyle.strokeColor(Color.BLACK);// 设置圆形的边框颜色
        myLocationStyle.radiusFillColor(Color.argb(100, 0, 0, 180));// 设置圆形的填充颜色
        myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_FOLLOW);
        // myLocationStyle.anchor(int,int)//设置小蓝点的锚点
        myLocationStyle.strokeWidth(1.0f);// 设置圆形的边框粗细
        aMap.setMyLocationStyle(myLocationStyle);
        aMap.setLocationSource(this);// 设置定位监听
        //aMap.getUiSettings().setMyLocationButtonEnabled(true);// 设置默认定位按钮是否显示
        aMap.setMyLocationEnabled(true);// 设置为true表示显示定位层并可触发定位，false表示隐藏定位层并不可触发定位，默认是false
        // aMap.setMyLocationType()
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //在activity执行onResume时执行mMapView.onResume ()，重新绘制加载地图
        mMapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //在activity执行onPause时执行mMapView.onPause ()，暂停地图的绘制
        mMapView.onPause();
        deactivate();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //在activity执行onSaveInstanceState时执行mMapView.onSaveInstanceState (outState)，保存地图当前的状态
        mMapView.onSaveInstanceState(outState);
    }

    @Override
    public void activate(OnLocationChangedListener onLocationChangedListener) {
        mListener = onLocationChangedListener;
        if (mlocationClient == null) {
            mlocationClient = new AMapLocationClient(this);
            mLocationOption = new AMapLocationClientOption();
            //设置定位监听
            mlocationClient.setLocationListener(this);
            //设置为高精度定位模式
            mLocationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);
            //设置定位参数
            mlocationClient.setLocationOption(mLocationOption);
            // 此方法为每隔固定时间会发起一次定位请求，为了减少电量消耗或网络流量消耗，
            // 注意设置合适的定位时间的间隔（最小间隔支持为2000ms），并且在合适时间调用stopLocation()方法来取消定位请求
            // 在定位结束后，在合适的生命周期调用onDestroy()方法
            // 在单次定位情况下，定位无论成功与否，都无需调用stopLocation()方法移除请求，定位sdk内部会移除
            mlocationClient.startLocation();
        }
    }

    @Override
    public void deactivate() {
        mListener = null;
        if (mlocationClient != null) {
            mlocationClient.stopLocation();
            mlocationClient.onDestroy();
        }
        mlocationClient = null;
    }

    @Override
    public void onLocationChanged(AMapLocation aMapLocation) {
        if (mListener != null && aMapLocation != null) {
            if (aMapLocation.getErrorCode() == 0) {
                mListener.onLocationChanged(aMapLocation);// 显示系统小蓝点
            } else {
                String errText = "定位失败," + aMapLocation.getErrorCode() + ": " + aMapLocation.getErrorInfo();
                Log.e("AmapErr", errText);
            }
        }
    }
}
