package com.black.board.ui.home;

import com.black.board.dagger.provide.FlowerModule;
import com.black.board.dagger.provide.PotModule;

import dagger.Component;

/**
 * 容器
 * Created by Administrator on 2017/11/17.
 */

@Component(modules ={FlowerModule.class , PotModule.class})
public interface MainActivityComponent {
    void inject(MainActivity activity);
}
