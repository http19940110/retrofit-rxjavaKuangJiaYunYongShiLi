package com.black.board.ui.home;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.black.board.R;
import com.black.board.base.BaseActivity;
import com.black.board.utils.AppManager;
import com.yanzhenjie.permission.AndPermission;

/**
 *
 * Created by gaojun on 2017/8/16.
 */

public class SplashActivity extends BaseActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        setPermission();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mContext.startActivity(new Intent(SplashActivity.this, MainActivity.class));
               AppManager.finishActivity(SplashActivity.this);
            }
        }, 1500);
    }


    /**
     * 检查权限
     */
    private void setPermission() {
        AndPermission.with(this)
                .requestCode(200)
                .permission(
                        // 多个权限，以数组的形式传入。
                        Manifest.permission.INTERNET,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.ACCESS_NETWORK_STATE,
                        Manifest.permission.ACCESS_WIFI_STATE,
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                ).start();
    }
}
