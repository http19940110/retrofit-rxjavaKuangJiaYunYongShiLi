package com.black.board.rx;


/**
 * 异常处理类 运行时异常
 */

public class ApiResultException extends RuntimeException {
    public ApiResultException() {
    }

    public ApiResultException(String detailMessage) {
        super(detailMessage);
    }

    public ApiResultException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public ApiResultException(Throwable throwable) {
        super(throwable);
    }
}
