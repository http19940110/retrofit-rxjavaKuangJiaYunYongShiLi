package com.black.board.rx;

import com.black.board.bean.NewResult;

import rx.Observable;
import rx.functions.Func1;

/**
 * 小黑板项目
 *
 * @param <T>
 */
public class ResultTransform<T> implements Func1<NewResult<T>, Observable<T>> {
    @Override
    public Observable<T> call(NewResult<T> result) {
        if (result.getCode().equals("success")) {
            return Observable.just(result.getData());//成功
        } else throw new ApiResultException(result.getMsg());//抛出异常
    }
}
