package com.black.board.rx;

import java.net.ConnectException;
import java.net.SocketTimeoutException;

import rx.Subscriber;

/**
 * 接口回调对错误onError进行替换
 *
 * @param <T>
 */

public class BaseSubscriber<T> extends Subscriber<T> {
    @Override
    public void onStart() {
        super.onStart();
        //进度条添加
    }

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {

        if (e instanceof ApiResultException) {

        } else throw new RuntimeException(e);
    }

    @Override
    public void onNext(T t) {

    }
}
