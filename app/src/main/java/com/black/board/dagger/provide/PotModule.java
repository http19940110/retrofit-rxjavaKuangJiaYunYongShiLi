package com.black.board.dagger.provide;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * 当做库使用 借助这个类提供对应限定符号标注的FlowerModule里面@Provide
 * Created by Administrator on 2017/11/17.
 */

@Module
public class PotModule {

    @Provides
    Pot providePot(@Named("flowerTwo") FlowerAbstract flower) {
        return new Pot(flower);
    }
}
