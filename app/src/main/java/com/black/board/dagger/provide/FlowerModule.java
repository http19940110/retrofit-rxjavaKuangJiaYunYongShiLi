package com.black.board.dagger.provide;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;

/**
 * 当做库使用 提供实例用的
 * Created by Administrator on 2017/11/17.
 */

@Module
public class FlowerModule {

    //提供花儿对象 抽象类实例
    @Provides
    @Named("flower")
    FlowerAbstract provideFlower() {
        return new Flower("我是第一朵花儿");
    }


    @Provides
    @Named("flowerTwo")
    FlowerAbstract provideFlowerTwo() {
        return new FlowerTwo("我是第二朵花儿");
    }
}
