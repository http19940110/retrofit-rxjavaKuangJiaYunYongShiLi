package com.black.board.dagger.provide;

/**
 * 抽象类不能用@Inject标注
 * Created by Administrator on 2017/11/17.
 */
//抽象类
public abstract class FlowerAbstract {
    public abstract String whisper();
}
