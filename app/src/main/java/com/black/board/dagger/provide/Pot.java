package com.black.board.dagger.provide;

import javax.inject.Inject;
import javax.inject.Named;

/**
 * 借助此类来判断提供什么module
 * Created by Administrator on 2017/11/17.
 */

public class Pot {
    private FlowerAbstract flower;

    @Inject
    public Pot( FlowerAbstract flower) {
        this.flower = flower;
    }

    public String show() {
        return flower.whisper();
    }

}
