package com.black.board.dagger.provide;



/**
 * 继承抽象类实现其方法
 * Created by Administrator on 2017/11/17.
 */

public class Flower extends FlowerAbstract {
    String name;

    public Flower(String name) {
        this.name=name;
    }

    @Override
    public String whisper() {
        return name;
    }
}
