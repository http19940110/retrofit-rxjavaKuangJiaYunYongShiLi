package com.black.board.dagger;

import javax.inject.Inject;

/**
 * Created by Administrator on 2017/11/17.
 */

public class DaggerBean {
    private DaggerTest daggerTest;


    //需要标注 代表注入此对象
    @Inject
    public DaggerBean(DaggerTest daggerTest) {
        this.daggerTest = daggerTest;
    }



    public String show() {
        return daggerTest.whisper();
    }
}
