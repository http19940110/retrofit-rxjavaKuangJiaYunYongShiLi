package com.black.board.bean;


/**
 * 电影测试公用类
 * Created by Administrator on 2017/8/16.
 */

import com.google.gson.annotations.SerializedName;

public class Result<T> {

    private final static int INVALID_CODE = -999;
    private final static int SUCCESS_CODE = 0;

    @SerializedName("success")
    private boolean success;
    @SerializedName("code")
    private int code = INVALID_CODE;

    @SerializedName("subjects")
    private T subjects;

    public boolean isSuccess() {
        //优先使用code进行判断，如果code值不满足则使用success
        //兼容后期只返回code而不返回success的情况
        return code == SUCCESS_CODE || success;
    }

    public Result setSuccess(boolean success) {
        this.success = success;
        return this;
    }


    public int getCode() {
        return code;
    }

    public Result setCode(int code) {
        this.code = code;
        return this;
    }

    public T getData() {
        return subjects;
    }

    public Result setData(T data) {
        this.subjects = data;
        return this;
    }
}

