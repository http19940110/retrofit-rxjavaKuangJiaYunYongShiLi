package com.black.board.bean;

/**
 * 用户注册
 * Created by Administrator on 2017/9/29.
 */

public class UserBean {


    /**
     * loginName : 1234567
     * loginPwd : 123456
     * userNickName : O.618*
     * memo : detail
     * createId : 0
     * isActive : 0
     * activeWay : 0
     * dataLevel : 0
     * gpsLng : 123.456
     * gpsLat : 456.789
     * loginAddr : address
     * associateMobile : 187...
     * associateEmail : 123@qq.com
     * age : 28
     * sex : 0
     * chatSetting :
     * isPublieAddr :
     * userIntegral : 0
     * isBindMobi : 0
     * isBindEmail : 0
     */

    private String loginName;
    private String loginPwd;
    private String userNickName;
    private String memo;
    private int createId;
    private int isActive;
    private int activeWay;
    private int dataLevel;
    private String gpsLng;
    private String gpsLat;
    private String loginAddr;
    private String associateMobile;
    private String associateEmail;
    private int age;
    private int sex;
    private String chatSetting;
    private String isPublieAddr;
    private int userIntegral;
    private int isBindMobi;
    private int isBindEmail;

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getLoginPwd() {
        return loginPwd;
    }

    public void setLoginPwd(String loginPwd) {
        this.loginPwd = loginPwd;
    }

    public String getUserNickName() {
        return userNickName;
    }

    public void setUserNickName(String userNickName) {
        this.userNickName = userNickName;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
    }

    public int getCreateId() {
        return createId;
    }

    public void setCreateId(int createId) {
        this.createId = createId;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public int getActiveWay() {
        return activeWay;
    }

    public void setActiveWay(int activeWay) {
        this.activeWay = activeWay;
    }

    public int getDataLevel() {
        return dataLevel;
    }

    public void setDataLevel(int dataLevel) {
        this.dataLevel = dataLevel;
    }

    public String getGpsLng() {
        return gpsLng;
    }

    public void setGpsLng(String gpsLng) {
        this.gpsLng = gpsLng;
    }

    public String getGpsLat() {
        return gpsLat;
    }

    public void setGpsLat(String gpsLat) {
        this.gpsLat = gpsLat;
    }

    public String getLoginAddr() {
        return loginAddr;
    }

    public void setLoginAddr(String loginAddr) {
        this.loginAddr = loginAddr;
    }

    public String getAssociateMobile() {
        return associateMobile;
    }

    public void setAssociateMobile(String associateMobile) {
        this.associateMobile = associateMobile;
    }

    public String getAssociateEmail() {
        return associateEmail;
    }

    public void setAssociateEmail(String associateEmail) {
        this.associateEmail = associateEmail;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getChatSetting() {
        return chatSetting;
    }

    public void setChatSetting(String chatSetting) {
        this.chatSetting = chatSetting;
    }

    public String getIsPublieAddr() {
        return isPublieAddr;
    }

    public void setIsPublieAddr(String isPublieAddr) {
        this.isPublieAddr = isPublieAddr;
    }

    public int getUserIntegral() {
        return userIntegral;
    }

    public void setUserIntegral(int userIntegral) {
        this.userIntegral = userIntegral;
    }

    public int getIsBindMobi() {
        return isBindMobi;
    }

    public void setIsBindMobi(int isBindMobi) {
        this.isBindMobi = isBindMobi;
    }

    public int getIsBindEmail() {
        return isBindEmail;
    }

    public void setIsBindEmail(int isBindEmail) {
        this.isBindEmail = isBindEmail;
    }
}
