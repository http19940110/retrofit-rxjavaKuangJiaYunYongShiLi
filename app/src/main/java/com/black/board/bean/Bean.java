package com.black.board.bean;

import java.util.List;

/**
 * Created by gaojun on 2017/7/31.
 */

public class Bean {
    /**
     * size : 1
     * mysql : [{"sql":"select * from black_board"}]
     */

    private int size;
    private List<MysqlBean> mysql;

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public List<MysqlBean> getMysql() {
        return mysql;
    }

    public void setMysql(List<MysqlBean> mysql) {
        this.mysql = mysql;
    }

    public static class MysqlBean {
        /**
         * sql : select * from black_board
         */

        private String sql;

        public String getSql() {
            return sql;
        }

        public void setSql(String sql) {
            this.sql = sql;
        }
    }
}
