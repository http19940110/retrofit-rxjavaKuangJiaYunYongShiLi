package com.black.board.bean;


/**
 * 滴滴打车专用公共结果返回类
 * Created by Administrator on 2017/9/13.
 */

public class NewResult<T> {

    /**
     * code : error
     * msg : result is empty
     * size : 0
     * result :
     */

    private String code;
    private String msg;
    private int size;

    private T result;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public T getData() {
        return result;
    }

    public NewResult setData(T data) {
        this.result = data;
        return this;
    }
}
