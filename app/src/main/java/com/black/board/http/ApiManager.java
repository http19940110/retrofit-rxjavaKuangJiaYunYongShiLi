package com.black.board.http;

import com.black.board.bean.Bean;
import com.black.board.bean.NewResult;
import com.black.board.bean.Person;
import com.black.board.bean.Result;
import com.black.board.bean.UserBean;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * 这里用来写网络请求方法 注解
 * 确定返回类型 根据后台类型来写
 * Created by gaojun on 2017/6/20.
 */

public interface ApiManager {

    //范例 例子
    @POST("device/select")
    Call<Object> getURl(@Body Bean str);


    //请求实例 例子
    @GET("v2/movie/top250?")
    Observable<Result<List<Person>>> getMovie(@Query("start") int start,
                                                 @Query("count") int count);

    //小黑板注册 范例
    @POST("device/user/reg")
    Observable<NewResult<String>> register(@Body UserBean str);//上传实体类


}