package com.black.board.http;



import com.black.board.BuildConfig;
import com.black.board.domain.Repository;
import com.black.board.domain.RepositoryImpl;
import com.black.board.utils.schedulers.AndroidSchedulerProvider;
import com.black.board.utils.schedulers.SchedulerProvider;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * 联网基础类
 */

public class ServiceManager {
    private static ServiceManager instance = null;
    public synchronized static ServiceManager getInstance() {
        return instance != null ? instance : new ServiceManager();
    }

    private OkHttpClient client = new OkHttpClient()
            .newBuilder()
            .addInterceptor(new HttpLoggingInterceptor()
                    .setLevel(BuildConfig.DEBUG ?
                            HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE))
            .build();

    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://api.douban.com/")//这是电影测试类的url
            //.baseUrl("http://219.141.203.69:8191/")//这个是小黑板
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
            .build();

    //api类绑定
    private ApiManager apiManager = retrofit.create(ApiManager.class);
    public ApiManager getApiManager() {
        return apiManager;
    }


    public SchedulerProvider getSchedulerProvider() {
        return new AndroidSchedulerProvider();
    }

    public Repository getRepository(ApiManager apiManager, SchedulerProvider schedulerProvider) {
        return new RepositoryImpl(apiManager, schedulerProvider);
    }

}
