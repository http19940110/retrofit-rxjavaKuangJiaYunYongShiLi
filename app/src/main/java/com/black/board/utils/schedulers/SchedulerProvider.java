package com.black.board.utils.schedulers;
/*
 * @description
 *   Please write the BaseSchedulerProvider module's description
 * @author rdshoep (rdshoep@126.com)
 *   http://www.rdshoep.com/
 * @version 
 *   1.0.0(16/10/25)
 */


import android.support.annotation.NonNull;

import rx.Scheduler;

  public interface SchedulerProvider {

    @NonNull
    Scheduler computation();

    @NonNull
    Scheduler io();

    @NonNull
    Scheduler ui();
}
