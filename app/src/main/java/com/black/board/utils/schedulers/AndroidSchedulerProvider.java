package com.black.board.utils.schedulers;
/*
 * @description
 *   Please write the SchedulerProvider module's description
 * @author rdshoep (rdshoep@126.com)
 *   http://www.rdshoep.com/
 * @version 
 *   1.0.0(16/10/25)
 */


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class AndroidSchedulerProvider implements SchedulerProvider {

    @Nullable
    private static AndroidSchedulerProvider INSTANCE;

    // Prevent direct instantiation.
    public AndroidSchedulerProvider() {
    }

    public static synchronized AndroidSchedulerProvider getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AndroidSchedulerProvider();
        }
        return INSTANCE;
    }

    @Override
    @NonNull
    public Scheduler computation() {
        return Schedulers.computation();
    }

    @Override
    @NonNull
    public Scheduler io() {
        return Schedulers.io();
    }

    @Override
    @NonNull
    public Scheduler ui() {
        return AndroidSchedulers.mainThread();
    }
}